var dsTheTd = document.querySelectorAll(".td-scores");
var dsTheTr = document.querySelectorAll("#tblBody tr");
var dsDiem = [];
for (var i = 0; i < dsTheTd.length; i++) {
  var diem = dsTheTd[i].innerText * 1;
  dsDiem.push(diem);
}
// nhận vào index và trả về tên tương ứng
function getName(viTri) {
  var theTr = dsTheTr[viTri];
  var dsTheTd = theTr.querySelectorAll("td");
  return dsTheTd[2].innerText;
}
function getScore(viTri) {
  var theTr = dsTheTr[viTri];
  var dsTheTd = theTr.querySelectorAll("td");
  return dsTheTd[3].innerText;
}

// tìm sinh viên giỏi nhất

function timSvGioiNhat() {
  var soLonNhat = dsDiem[0];
  for (var i = 0; i < dsDiem.length; i++) {
    if (dsDiem[i] > soLonNhat) {
      soLonNhat = dsDiem[i];
    }
  }
  var viTri = dsDiem.indexOf(soLonNhat);
  var ten = getName(viTri);
  var diem = getScore(viTri);
  console.log({ ten, diem });
  document.getElementById("svGioiNhat").innerText = ` ${ten} ${diem}`;
}
timSvGioiNhat();
//
function timSvYeuNhat() {
  /**
   * 1. tìm giá trị của số bé nhất trong array điểm
   * 2. từ giá trị nhỏ nhất=> tìm ra index ( vị trí )
   * 3. show kết quả
   */
  var soNhoNhat = dsDiem[0];
  for (var i = 0; i < dsDiem.length; i++) {
    if (dsDiem[i] < soNhoNhat) {
      soNhoNhat = dsDiem[i];
    }
  }
  var viTri = dsDiem.indexOf(soNhoNhat);
  var ten = getName(viTri);
  var diem = getScore(viTri);
  document.getElementById("svYeuNhat").innerText = ` ${ten} - ${diem}`;
}
timSvYeuNhat();
