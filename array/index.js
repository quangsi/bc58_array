// pass by value : string, number, boolean

// pass by reference: array , object

var colors = ["red", "blue"];

// số lượng phần tử hiện có
console.log(colors.length);

// thêm 1 phần tử vào danh sách
colors.push("black");
console.log("😀 - colors", colors);

// update giá trị của 1 phần tử
colors[2] = "supper black";
console.log("😀 - colors", colors);

// CRUD ~ Create Read Update Delete

// splice slice mdn
// splic( vị trí cần xoá, số lượng )
colors.splice(0, 1);
console.log("😀 - colors", colors);

var menu = ["bún bò", "cơm tấm", "hủ tiếu"];
// thêm món ăn
menu.push("bánh canh cua Cà Mau");
console.log("😀 - menu sau khi thêm món bánh canh cua", menu);

// update món "bún bò" thành "bún bò Huế"
menu[0] = "Bún bò Huế";
console.log("😀 - menu sau khi update món bún bò", menu);

// tìm kiếm 1 món ăn trong danh sách món ăn dựa trên keyword

var keyword = "Bún  bò Huế";
// duyệt mảng
var flag = -1;
for (var i = 0; i < menu.length; i++) {
  if (menu[i] == keyword) {
    flag = i;
  }
}
// kiểm tra có tìm thấy món ăn hay không
if (flag == -1) {
  console.log("Ko tìm thấy");
} else {
  console.log("Tìm thấy");
}
// xoá món hủ tiếu ra khỏi memu => splice

var viTriHuTieu = menu.indexOf("hủ tiếu");
// for (var i = 0; i < menu.length; i++) {
//   if (menu[i] == "hủ tiếu") {
//     viTriHuTieu = i;
//   }
// }
console.log("😀 - viTriHuTieu", viTriHuTieu);
// xoá phần tử tại index = 2 trong array
menu.splice(viTriHuTieu, 1);
console.log("😀 - menu sau khi xoá món hủ tiếu", menu);

// indexOf w3
